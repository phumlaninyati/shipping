<?php

class Tnma_Skynet_Model_Carrier
    extends Mage_Shipping_Model_Carrier_Abstract
//    implements Mage_Shipping_Model_Carrier_Interface
{

    protected $_code = 'tnma_skynet';

    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {

        $result = Mage::getModel('shipping/rate_result');
        /* @var $result Mage_Shipping_Model_Rate_Result */

        $customerAddressId = Mage::getSingleton('customer/session')->getCustomer()->getDefaultShipping();
        if ($customerAddressId) {
            $address = Mage::getModel('customer/address')->load($customerAddressId);
            $address->getData();
        }


//        $result->append($this->_getStandardShippingRate());
        //$result->append($this->_getRealtimeShippingRate());

        // Get cart items and put them in an Array or quit
        $items = $request->getAllItems();
        if ($items) {
            $cart = $this->get_cart_content($items);
        } else {
            return false;
        }

        $url = Mage::getStoreConfig('carriers/tnma_skynet/live_skynet_url');
        $proxy = new SoapClient($url, array("exceptions" => 1, 'trace' => 1));
        $token = $this->logon();
        $serviceTypes = array('on1', 'on2');
        $myrates = array();
        foreach ($serviceTypes as $serviceType) {
            $token = $this->logon();
            $myrates[] = $this->doRateLookup($token, $cart, $serviceType, $insuranceAmount = "0", $insuranceType = 'N', $address);
        }

        $i = 0;
        foreach ($myrates as $myrate) {
            $i++;

            $method = Mage::getModel('shipping/rate_result_method');
            // Record carrier information
//            $method->setCarrier($this->_code);
//            $method->setCarrierTitle('Skynet');
            // Record method information
            if ($i == 1) {
                $method->setCarrier($this->_code);
                $method->setCarrierTitle('Skynet Express');
                $method->setMethod('ON1');
                $method->setMethodTitle('Over Night - by 10:30');
            } else {
                $method->setCarrier($this->_code);
                $method->setCarrierTitle('Skynet Standard');
                $method->setMethod('ON2');
                $method->setMethodTitle('Over Night - by 13:00');
            }

            // Record how much it costs to vendor to ship

            // Price the client will be charged
            $method->setPrice($myrate);
            // Add this rate to the result
            $result->append($method);
        }
        return $result;
//
//            $rate->setCarrier($this->_code);
//            $rate->setCarrierTitle($this->getConfigData('title'));
//
//            $rate->setMethod('standand');
//            if ($serviceType == 'ON1'){
//
//
//            $rate->setMethodTitle('Over Night Express');
//            }
//            else {
//                $rate->setMethodTitle('Over Night 3-5 Days');
//            }
//            $rate->setPrice($myrate);
//        }
        //$services = $proxy->__soapCall('GetQuote', array('serviceType' => 2));

//        $rate = Mage::getModel('shipping/rate_result_method');

//        $rate->setCost(0);


//        $services = $this->get_services();
//
//        foreach ($services as $key => $value) {
//            $i = $this->doRateLookup($key, $cart);
//            if ($i > 1) {
//                $response[] =
//                    Array(
//                        'code' => $key,
//                        'title' => $value,
//                        'cost' => $i,
//                        'price' => $i * (1 + ($this->getConfigData('markup') / 100)),
//                    );
//            }
//
//        }
//
//        $result = Mage::getModel('shipping/rate_result');
//        foreach ($response as $rMethod) {
//            $method = Mage::getModel('shipping/rate_result_method');
//            // Record carrier information
//            $method->setCarrier($this->_code);
//            $method->setCarrierTitle($this->getConfigData('title'));
//            // Record method information
//            $method->setMethod($rMethod['code']);
//            $method->setMethodTitle($rMethod['title']);
//            // Record how much it costs to vendor to ship
//            $method->setCost($rMethod['cost']);
//            // Price the client will be charged
//            $method->setPrice($rMethod['price']);
//            // Add this rate to the result
//            $result->append($method);
//        }
//        return $result;
    }

//    protected function _getStandardShippingRate() {
//
//        $rate = Mage::getModel('shipping/rate_result_method');
//        /* @var $rate Mage_Shipping_Model_Rate_Result_Method */
//
//        $rate->setCarrier($this->_code);
//        /**
//         * getConfigData(config_key) returns the configuration value for the
//         * carriers/[carrier_code]/[config_key]
//         */
//        $rate->setCarrierTitle($this->getConfigData('title'));
//
//        $rate->setMethod('standand');
//        $rate->setMethodTitle('Standard');
//
//        $rate->setPrice(4.99);
//        $rate->setCost(0);
//
//        return $rate;
//    }

//    protected function _getExpressShippingRate() {
//
//        $rate = Mage::getModel('shipping/rate_result_method');
//        /* @var $rate Mage_Shipping_Model_Rate_Result_Method */
//        $rate->setCarrier($this->_code);
//        $rate->setCarrierTitle($this->getConfigData('title'));
//        $rate->setMethod('express');
//        $rate->setMethodTitle('Express (Next day)');
//        $rate->setPrice(12.99);
//        $rate->setCost(0);
//
//        return $rate;
//    }

//    protected function _getRealtimeShippingRate() {
//
//        $rate = Mage::getModel('shipping/rate_result_method');
//        /* @var $rate Mage_Shipping_Model_Rate_Result_Method */
//        $rate->setCarrier($this->_code);
//        $rate->setCarrierTitle($this->getConfigData('title'));
//        $rate->setMethod('realtime');
//        $rate->setMethodTitle('Realtime Web Rate');
//
//        // do rate lookup via api
//        $cost = $this->getRateViaApi();
//
//        $rate->setPrice(77.77);
//        $rate->setCost(0);
//
//        return $rate;
//    }

//    protected function _getFreeShippingRate() {
//
//        $rate = Mage::getModel('shipping/rate_result_method');
//        /* @var $rate Mage_Shipping_Model_Rate_Result_Method */
//        $rate->setCarrier($this->_code);
//        $rate->setCarrierTitle($this->getConfigData('title'));
//        $rate->setMethod('free_shipping');
//        $rate->setMethodTitle('Free Shipping (3 - 5 days)');
//        $rate->setPrice(0);
//        $rate->setCost(0);
//
//        return $rate;
//    }

//    public function getAllowedMethods() {
//
//        return array(
//            'standard' => 'Standard',
//            'express' => 'Express',
//            'realtime' => 'Realtime',
//            'free_shipping' => 'Free Shipping',
//        );
//    }

//    public function getRateViaApi() {
//
//        // logon
//        $token = $this->logon();
//
//        // get rates
//        if ($token) {
//            // do rate lookup
//            $rate = $this->doRateLookup($token);
//
//        } else {
//            return false;
//        }
//
//    }

    public function logon()
    {

        $testmode = Mage::getStoreConfig('carriers/tnma_skynet/testmode');

        if ($testmode) {
            $url = Mage::getStoreConfig('carriers/tnma_skynet/test_skynet_url');
            $username = Mage::getStoreConfig('carriers/tnma_skynet/test_skynet_user');
            $password = Mage::getStoreConfig('carriers/tnma_skynet/test_skynet_password');
        } else {
            $url = Mage::getStoreConfig('carriers/tnma_skynet/live_skynet_url');
            $username = Mage::getStoreConfig('carriers/tnma_skynet/live_skynet_user');
            $password = Mage::getStoreConfig('carriers/tnma_skynet/live_skynet_password');
        }

        // connect to api to get rates
        try {
            $params2 = array(
                'userName' => $username,
                'password' => $password,
            );

            //make the call
            $proxy = new SoapClient($url, array("exceptions" => 1, 'trace' => 1));
            $token = $proxy->__soapCall('Logon', array('parameters' => $params2));
            $result = $token->LogonResult;

            if ($result) {
                return $result;
            } else {
                Mage::log('Could not get token');

                return false;
            }

        } catch (SoapFault $e) {

            Mage::log($e->faultstring);

        }

    }

    function get_cart_content($items)
    {
        // Reset array to defaults
        $cart = array(
            'count' => 0,
            'weight' => 0,
            'parcels' => Array()
        );
        // Loop through every product in the cart
        foreach ($items as $item) {
            if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                continue;
            }
            // If the product has children and is shipped seperately, get info for each child product
            if ($item->getHasChildren() && $item->isShipSeparately()) {
                foreach ($item->getChildren() as $child) {
                    if ($child->getFreeShipping() && !$child->getProduct()->isVirtual()) {
                        $product_id = $child->getProductId();
                        $productObj = Mage::getSingleton('catalog/product')->load($product_id);
                        // Get info from product
                        $qty = $child->getQty();
                        $weight = $productObj->getData('ufiiunitweight');
                        $width = $productObj->getData('ufiiunitwidth');
                        $length = $productObj->getData('ufiiunitdepth');
                        $height = $productObj->getData('ufiiunitheight');
                        if ($weight == "0" || $width == "0" || $length == "0" || $height == "0") {
                            $weight = 1;
                            $width = 500;
                            $length = 500;
                            $height = 500;
                        }
                        $cart['count'] += $qty;
                        $cart['weight'] += $weight * $qty;
                        for ($i = 0; $i < $qty; $i++) {
                            $cart['parcels'][] = array(
                                'length' => $length,
                                'width' => $width,
                                'height' => $height,
                                'weight' => $weight,
                            );
                        }
                    }
                }
                // Else get the data for current product
            } else {
                $product_id = $item->getProductId();
                $productObj = Mage::getSingleton('catalog/product')->load($product_id);
                // Get info from product
                $qty = $item->getQty();
                if (!isset($qty)) {
                    $qty = $item->getQtyOrdered();
                }
                $weight = $productObj->getData('ufiiunitweight');
                $width = $productObj->getData('ufiiunitwidth');
                $length = $productObj->getData('ufiiunitdepth');
                $height = $productObj->getData('ufiiunitheight');
                if ($weight == "0" || $width == "0" || $length == "0" || $height == "0") {
                    $weight = 1;
                    $width = 50;
                    $length = 50;
                    $height = 50;
                }
                $cart['count'] += $qty;
                $cart['weight'] += $weight * $qty;
                for ($i = 0; $i < $qty; $i++) {
                    $cart['parcels'][] = array(
                        'length' => $length,
                        'width' => $width,
                        'height' => $height,
                        'weight' => $weight,
                    );
                }
            }
        }
        return $cart;
    }


    public function doRateLookup($token, $cart, $serviceType, $insuranceAmount = "0", $insuranceType = 'N', $address)
    {
        $shipping_address = strtoupper($address->getData('city'));
        $postC = $address->getData('postcode');
        $toCity = $shipping_address;
        $postCode = $postC;
        $fromCity = Mage::getStoreConfig('shipping/origin/city');
        $accNumber = 'D15159';


        $params = array(
            'logonToken' => $token,
            'accountNumber' => $accNumber,
            'fromCity' => "CAPE TOWN CBD", //"CAPE TOWN CBD",$fromCity
            'toCity' => $toCity,
            'serviceType' => $serviceType,
            'insuranceType' => $insuranceType,
            'insuranceAmount' => $insuranceAmount,
            'DestinationPCode' => $postCode,

        );
        $params['Prcl1_NrOfParcels'] = '';
        $params['Prcl1_Length'] = '';
        $params['Prcl1_Width'] = '';
        $params['Prcl1_Height'] = '';
        $params['Prcl1_Weight'] = '';
        $params['Prcl2_NrOfParcels'] = '';
        $params['Prcl2_Length'] = '';
        $params['Prcl2_Width'] = '';
        $params['Prcl2_Height'] = '';
        $params['Prcl2_Weight'] = '';
        $params['Prcl3_NrOfParcels'] = '';
        $params['Prcl3_Length'] = '';
        $params['Prcl3_Width'] = '';
        $params['Prcl3_Height'] = '';
        $params['Prcl3_Weight'] = '';
        $params['Prcl3_NrOfParcels'] = '';
        $params['Prcl3_Length'] = '';
        $params['Prcl3_Width'] = '';
        $params['Prcl3_Height'] = '';
        $params['Prcl3_Weight'] = '';
        $params['Prcl4_NrOfParcels'] = '';
        $params['Prcl4_Length'] = '';
        $params['Prcl4_Width'] = '';
        $params['Prcl4_Height'] = '';
        $params['Prcl4_Weight'] = '';
        $params['Prcl5_NrOfParcels'] = '';
        $params['Prcl5_Length'] = '';
        $params['Prcl5_Width'] = '';
        $params['Prcl5_Height'] = '';
        $params['Prcl5_Weight'] = '';
        $params['Prcl6_NrOfParcels'] = '';
        $params['Prcl6_Length'] = '';
        $params['Prcl6_Width'] = '';
        $params['Prcl6_Height'] = '';
        $params['Prcl6_Weight'] = '';
        $params['Prcl7_NrOfParcels'] = '';
        $params['Prcl7_Length'] = '';
        $params['Prcl7_Width'] = '';
        $params['Prcl7_Height'] = '';
        $params['Prcl7_Weight'] = '';
        $params['Prcl8_NrOfParcels'] = '';
        $params['Prcl8_Length'] = '';
        $params['Prcl8_Width'] = '';
        $params['Prcl8_Height'] = '';
        $params['Prcl8_Weight'] = '';
        $params['Option1'] = '';
        $params['Option2'] = '';
        $params['Option3'] = '';
        $params['Option4'] = '';
        $params['Option5'] = '';
        $params['Option6'] = '';
        $params['Option7'] = '';
        $params['Option8'] = '';


        $i = 0;
        $parcelstnma = $cart['parcels'];

        foreach ($parcelstnma as $parcel) {
            $i++;
            $params["Prcl" . $i . "_NrOfParcels"] = 1;
            $params["Prcl" . $i . "_Length"] = $parcel['length'];
            $params["Prcl" . $i . "_Width"] = $parcel['width'];
            $params["Prcl" . $i . "_Height"] = $parcel['height'];
            $params["Prcl" . $i . "_Weight"] = $parcel['weight'];

        }


        //make the call
        $url = Mage::getStoreConfig('carriers/tnma_skynet/live_skynet_url');
        $proxy = new SoapClient($url, array("exceptions" => 1, 'trace' => 1));
        $quote = $proxy->__soapCall('GetQuote', array('parameters' => $params));

//            $services[] = $quote['serviceType'];

        if ($quote) {
            $quote = $quote->sCharges;
            return $quote;
        } else {
            Mage::log('Could not get rate');

            return false;
        }

    }
//catch
//(SoapFault $e)
//{
//
//Mage::log($e->faultstring);
//
//}

//This function should be diregarded
    function get_services($quote)
    {
        $testmode = Mage::getStoreConfig('carriers/tnma_skynet/testmode');

        if ($testmode) {
            $url = Mage::getStoreConfig('carriers/tnma_skynet/test_skynet_url');
            $username = Mage::getStoreConfig('carriers/tnma_skynet/test_skynet_user');
            $password = Mage::getStoreConfig('carriers/tnma_skynet/test_skynet_password');
        } else {
            $url = Mage::getStoreConfig('carriers/tnma_skynet/live_skynet_url');
            $username = Mage::getStoreConfig('carriers/tnma_skynet/live_skynet_user');
            $password = Mage::getStoreConfig('carriers/tnma_skynet/live_skynet_password');
        }
        $proxy = new SoapClient($url, array("exceptions" => 1, 'trace' => 1));
        $services = $proxy->__soapCall('GetQuote', array('serviceType' => '2'));


        if (!isset($this->services)) {
            try {

                $services = $quote['serviceType'];
                if (is_array($services['services']) && !isset($services['error'])) {
                    $this->services = $services['services'];
                } else {
                    $this->log("Error returning services! Recieved: " . $services, 3);
                    return false;
                }
            } catch (SoapFault $e) {
                $this->log("Error returning services! SoapFault: " . $e->faultcode . " - " . $e->getMessage(), 2);
                return false;
            }
        }
        return $this->services;

    }

    public function skynet()
    {
        return array($this->_code => 'Tnma Skynet');
    }


    private function log($text, $level = null)
    {
        Mage::log($text, $level, 'tnma_skynet.log');
    }


}


